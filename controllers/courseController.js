const Course = require('../models/Course');

// Create a New Course
module.exports.addCourse = (requestBody) => {

	let newCourse = new Course ({

		name: requestBody.name,
		description: requestBody.description,
		price: requestBody.price
	})

	return newCourse.save().then((course, error) => {

		if(error) {

			return false
		} else {

			return true
		}
	})
}


// Retrieve all courses
module.exports.getAllCourse = () => {

	return Course.find({}).then (result => {

		return result;
	})
}

// Retrieve ACTIVE course

module.exports.getAllActive = () => {

	return  Course.find({isActive: true}).then (result => {

		return result;
	})
}

// Retrieving a specific course
module.exports.getCourse = (requestParams) => {

	return Course.findById(requestParams.courseId).then(result => {
		return result;
	})
}

/*
// Updating course
module.exports.updateCourse = (requestParams, requestBody, data) => {

	if(data.isAdmin === true) {

		let updatedCourse = {
			name: requestBody.name,
			description: requestBody.description,
			price: requestBody.price
		};

		return Course.findByIdAndUpdate(requestParams.courseId, updateCourse).then((course, error) => {

			if (error) {
				return false
			} else {
				return true
			}
		})
	} else {

		return false
	}
}
*/

// Controller for updating a record
module.exports.updateCourse = (requestParams, requestBody, data) => {
	
	if (data.isAdmin === true) {
		let updatedCourse = {
			name: requestBody.name,
			description: requestBody.description,
			price: requestBody.price
		}
	
		return Course.findByIdAndUpdate(requestParams.courseId, updatedCourse).then((course, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		})

	} else {
		return false
	}
}

// Activity 
// 2. Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.

module.exports.archiveCourse = (requestParams, requestBody, data) => {

	if (data.isAdmin === true) {
		let archivedCourse = {
			isActive: requestBody.isActive
		}

		return Course.findByIdAndUpdate(requestParams.courseId, archivedCourse).then((course,error) => {

			if(error){
				return false
			} else {
				return true
			}
		})
	} else {

		return false
	}
}